#########################################################################################################################
############################### KOALA HABITAT DATA READ CHECK ###########################################################
#########################################################################################################################


#########################################################################################################################
## create a skeleton of the workflow, fill in the details iteratively


## setup
## maxent packages: note you need to match the version of Java on the computer (e.g. 64 bit)
## with the java version used by R.


# install.packages("ENMeval")
# install.packages("mctest")
# install.packages("rJava")
# install.packages("statisticalModeling")
# system.file("java", package = "dismo")

#library(ENMeval)
#library(rJava)  ## need to get 64 bit java again, 64 bit R 
library(mctest)
library(faraway)
library(DAAG)

library(raster)
library(spdep)
library(rgdal)
library(GISTools)

library(data.table)
library(plyr)
library(dtplyr)
#library(dplyr)
library(Hmisc)
library(Cairo)
require(lattice)
library(gtools)
library(ggplot2)
library(sjPlot)
library(gridExtra)
library(grid)
library(sqldf)
library(knitr)
library(statisticalModeling)
library(sm)

library(mgcv)
library(MuMIn)
library(mvabund)
library(asbio)
require(car)

library(mvtnorm)
library(testthat)
library(rgl)
library(R.matlab)
library(rgdal)
library(gtools)
library(formula.tools)


## source some functions that might be useful
source("./R/WT_SPECIES_FUNCTIONS.R")
source("./R/WT_REGRESSION_FUNCTIONS.R")





#########################################################################################################################
## 1). Read in Koala occurreces and study area
#########################################################################################################################


## Using the DPI koala records 
## N:\NVIS_DATA\KoalaSiteData\DPI\samples_2km_1.shp


## koala occurrences, sampled five times. Here I incude all five samples, and we can subset later 
KOALA.SHP                = readOGR("./data/base/sites/samples_2km_merge.shp",  layer = "samples_2km_merge")
STUDY.AREA               = readOGR("./data/base/sites/study_area_polygon.shp", layer = "study_area_polygon")
proj4string(KOALA.SHP)
proj4string(STUDY.AREA)


## subset points to just the study area. ArcMap says there are about 35 points that are not in the study area
KOALA.SITES.STUDY <- KOALA.SHP[STUDY.AREA, ]  ## this keeps the attributes
dim(KOALA.SHP)[1]  - dim(KOALA.SITES.STUDY)[1]
View(KOALA.SITES.STUDY)


## update field names
write.csv(as.data.frame(KOALA.SITES.STUDY), "./data/base/sites/samples_2km_merge.csv", row.names = FALSE)
KOALA.SITES.CONTINUOUS   = read.csv("./data/base/sites/Koala_point_samples_clean.csv", stringsAsFactors = FALSE)


## check: all the continuous NVIS layers
dim(KOALA.SITES.CONTINUOUS)
names(KOALA.SITES.CONTINUOUS)
str(KOALA.SITES.CONTINUOUS)


## Check some of the extracted raster values make sense across the list
summary(KOALA.SITES.CONTINUOUS$ce_radann)
summary(KOALA.SITES.CONTINUOUS$rs_waterobs)
summary(KOALA.SITES.CONTINUOUS$dl_lat_grid)
summary(KOALA.SITES.CONTINUOUS$sw_weath_ind)  ## all look ok


## Now get rid of the columns which have zero, and potentially sort the characters in R too
KOALA.VAR.CONTINUOUS = KOALA.SITES.CONTINUOUS[c(2:163)]
KOALA.VAR.CONTINUOUS = KOALA.VAR.CONTINUOUS[, colSums(KOALA.VAR.CONTINUOUS != 0) > 0]
dim(KOALA.VAR.CONTINUOUS)


## here could read in layer metadata: don't hae this for all the layers, but would be handy!
LAYER.METADATA = read.csv("./data/base/raster/nvis_raster_info.csv", stringsAsFactors = FALSE)
LAYER.GROUPS   = LAYER.METADATA[c("Group", "LayerName")]
head(LAYER.METADATA)


## now create a list of layers which have no metadata, to send to Adam:
layer.list      = read.csv("./data/base/raster/layer_list.csv",     stringsAsFactors = FALSE)
directory.list  = read.csv("./data/base/raster/directory_list.csv", stringsAsFactors = FALSE)


## now create atomic vectors that set operations can work with
layer.list      = layer.list[['Layer_list']]
directory.list  = directory.list[['Directory_list']]
head(layer.list)     ## length(layer.list)
head(directory.list) ## length(directory.list)


## now get the intersection and difference!
layer.list.intersect = intersect(layer.list, directory.list)
layer.list.diff      = setdiff(directory.list, layer.list)
length(layer.list.intersect) ## layer.list.intersect
length(layer.list.diff)      ## layer.list.diff


## save the setdist to file
layer.list.diff = as.data.frame(layer.list.diff)
#write.csv(as.data.frame(layer.list.diff), "./data/base/raster/layers_without_descriptions.csv", row.names = FALSE)
#l

## create points for extraction
SITES           = data.matrix(KOALA.VAR.CONTINUOUS[1:3])
SITES.points    = data.matrix(KOALA.VAR.CONTINUOUS[1:2])
SITES.points.df = as.data.frame(SITES.points)


## plot points over forested areas
## https://gis.stackexchange.com/questions/36877/how-do-i-change-the-polygon-fill-color-and-border-color-for-spatialpolygons-obje
plot(STUDY.AREA, col = "gray48", border = NA)
points(SITES, cex = 0.2, col = "blue", pch = 19)





#########################################################################################################################
## 2). Create pairwise corrleations between all layers
#########################################################################################################################


## now can we look at a corrleation matrix for all the variables, across all samples? Messy, but we need to look 
## _between_ categories, as well as _within_. Some can be ruled out a-priori, e.g. not interested in climate, really just 
## after layers that can contribute meaningful info. 
## EG: topographic position, tree distribution, fertility, fragmentation indices


## create pearson correlations between all variables
## Could select only those variablies with a correlation less than 0.7 or 0.4?
## many of the correlations will be non-linear too.
ALL.COR.SORT = cor(KOALA.VAR.CONTINUOUS[c(4:146)])
ALL.COR.SORT[lower.tri(ALL.COR.SORT, diag = TRUE)] = NA       # Prepare to drop duplicates and meaningless information
ALL.COR.SORT = as.data.frame(as.table(ALL.COR.SORT))          # Turn into a 3-column table
ALL.COR.SORT = na.omit(ALL.COR.SORT)                          # Get rid of the junk
ALL.COR.SORT = ALL.COR.SORT[order(-abs(ALL.COR.SORT$Freq)),]  # Sort by highest correlation (whether +ve or -ve)


## rename : don't need the permutation number
names(ALL.COR.SORT)[names(ALL.COR.SORT)=="Var1"] <- "LayerName"
names(ALL.COR.SORT)[names(ALL.COR.SORT)=="Var2"] <- "Layer_2"
names(ALL.COR.SORT)[names(ALL.COR.SORT)=="Freq"] <- "Pearson_R2"


## check
dim(ALL.COR.SORT)
head(ALL.COR.SORT)
View(ALL.COR.SORT)


## could join the groups on to the correlation table, but this assumes both variables
## are from the same group? For correlations above 0.4, this seems to be true!
ALL.COR.SORT.GROUPS = join(ALL.COR.SORT, LAYER.GROUPS, by = "LayerName", type = "full", match = "all")
dim(ALL.COR.SORT.GROUPS)  ## the difference is those variables without groups...
dim(ALL.COR.SORT)
head(ALL.COR.SORT.GROUPS, 20)
tail(ALL.COR.SORT.GROUPS, 20)


## now can subset based on correlation? Interesting just to see 
## which variables are correlated or not, ignoring categories
ALL.COR.ABOVE.0.7 = subset(ALL.COR.SORT.GROUPS, abs(Pearson_R2) >= 0.7) ## 271  combinations
ALL.COR.BELOW.0.7 = subset(ALL.COR.SORT.GROUPS, abs(Pearson_R2) <= 0.7) ## 9882 combinations
ALL.COR.ABOVE.0.4 = subset(ALL.COR.SORT.GROUPS, abs(Pearson_R2) >= 0.4) ## 852 combinations
ALL.COR.BELOW.0.4 = subset(ALL.COR.SORT.GROUPS, abs(Pearson_R2) <= 0.4) ## 9301 combinations


## check the matching...
tail(ALL.COR.ABOVE.0.7)
tail(ALL.COR.ABOVE.0.4)
tail(ALL.COR.BELOW.0.4)


## reorder
ALL.COR.SORT.GROUPS = ALL.COR.SORT.GROUPS[c("Group", "LayerName", "Layer_2", "Pearson_R2")]


## can write to file
# write.csv(ALL.COR.SORT.GROUPS, "./output/tables/ALL_CONTINUOUS_CORRELATIONS.csv", row.names = FALSE)





#########################################################################################################################
## 3). subset data, run pairwise corrleations and create scatterplot matrices for target groups
#########################################################################################################################


## need to consider the subsets, where these happen...
KOALA.VAR.CONTINUOUS.1 = subset(KOALA.VAR.CONTINUOUS, Sample == 1)
KOALA.VAR.CONTINUOUS.2 = subset(KOALA.VAR.CONTINUOUS, Sample == 2)
KOALA.VAR.CONTINUOUS.3 = subset(KOALA.VAR.CONTINUOUS, Sample == 3)
KOALA.VAR.CONTINUOUS.4 = subset(KOALA.VAR.CONTINUOUS, Sample == 4)
KOALA.VAR.CONTINUOUS.5 = subset(KOALA.VAR.CONTINUOUS, Sample == 5)


## not all exactly the same size...
dim(KOALA.VAR.CONTINUOUS.1)
dim(KOALA.VAR.CONTINUOUS.5)


## for all subsets, do the correlation. Ideally, this will be a for loop but just get it working
S1.CONTINUOUS.COR = cor(KOALA.VAR.CONTINUOUS.1[c(4:146)])


## sort correlation matrix
S1.COR.SORT = S1.CONTINUOUS.COR
S1.COR.SORT[lower.tri(S1.COR.SORT, diag = TRUE)] = NA       # Prepare to drop duplicates and meaningless information
S1.COR.SORT = as.data.frame(as.table(S1.COR.SORT))          # Turn into a 3-column table
S1.COR.SORT = na.omit(S1.COR.SORT)                          # Get rid of the junk
S1.COR.SORT = S1.COR.SORT[order(-abs(S1.COR.SORT$Freq)),]  # Sort by highest correlation (whether +ve or -ve)


## rename : don't need the permutation number
names(S1.COR.SORT)[names(S1.COR.SORT)=="Var1"] <- "LayerName"
names(S1.COR.SORT)[names(S1.COR.SORT)=="Var2"] <- "Layer_2"
names(S1.COR.SORT)[names(S1.COR.SORT)=="Freq"] <- "Pearson_R2"


## could join the groups on to the correlation table, but this assumes both variables
## are from the same group? For correlations above 0.4, this seems to be true!
S1.COR.SORT.GROUPS = join(S1.COR.SORT, LAYER.GROUPS, by = "LayerName", type = "full", match = "all")
dim(S1.COR.SORT.GROUPS)  ## the difference is those variables without groups...
dim(S1.COR.SORT)
S1.COR.SORT.GROUPS = S1.COR.SORT.GROUPS[c("Group", "LayerName", "Layer_2", "Pearson_R2")] 
head(S1.COR.SORT.GROUPS, 20)
tail(S1.COR.SORT.GROUPS, 20)


## now can subset based on correlation? Interesting just to see 
## which variables are correlated or not, ignoring categories
S1.COR.ABOVE.0.7 = subset(S1.COR.SORT.GROUPS, abs(Pearson_R2) >= 0.7) ## 271  combinations
S1.COR.BELOW.0.7 = subset(S1.COR.SORT.GROUPS, abs(Pearson_R2) <= 0.7) ## 9882 combinations
S1.COR.ABOVE.0.4 = subset(S1.COR.SORT.GROUPS, abs(Pearson_R2) >= 0.4) ## 852 combinations
S1.COR.BELOW.0.4 = subset(S1.COR.SORT.GROUPS, abs(Pearson_R2) <= 0.4) ## 9301 combinations


## check the matching...
tail(S1.COR.ABOVE.0.7)
tail(S1.COR.ABOVE.0.4)
tail(S1.COR.BELOW.0.4)


## write to file
# write.csv(S1.COR.SORT, "./output/tables/ALL_CONTINUOUS_CORRELATIONS.csv", row.names = FALSE)





#########################################################################################################################
## Now create pairwise correlations and Plot, but only for variable groups of interest. 
#########################################################################################################################


## Not really interested in climate, only those that have changed and can shift the MAXENT response curve. 
## TARGET CATEGORIES :

## topo, 
## tree distribution
## fertilty
## fragmentation


#########################################################################################################################
## LANDFORM: TOPOGRAPHIC POSITION
## here's a simple way to select only the columns matching a particular group:
KOALA.SITES.1.LANDFORM = KOALA.VAR.CONTINUOUS.1[,grep("^lf", colnames(KOALA.VAR.CONTINUOUS.1))]
names(KOALA.SITES.1.LANDFORM)


## pearson correlation for all topographic position indices
K1.LF.TPI.COR = cor(KOALA.VAR.CONTINUOUS.1[,grep("^lf_tpi", colnames(KOALA.VAR.CONTINUOUS.1))])


## sort correlation matrix
K1.LF.TPI.COR[lower.tri(K1.LF.TPI.COR, diag = TRUE)] = NA     # Prepare to drop duplicates and meaningless information
K1.LF.TPI.COR = as.data.frame(as.table(K1.LF.TPI.COR))        # Turn into a 3-column table
K1.LF.TPI.COR = na.omit(K1.LF.TPI.COR)                        # Get rid of the junk
K1.LF.TPI.COR = K1.LF.TPI.COR[order(-abs(K1.LF.TPI.COR$Freq)),]   # Sort by highest correlation (whether +ve or -ve)


## rename : don't need the permutation number
names(K1.LF.TPI.COR)[names(K1.LF.TPI.COR)=="Var1"] <- "LayerName"
names(K1.LF.TPI.COR)[names(K1.LF.TPI.COR)=="Var2"] <- "Layer_2"
names(K1.LF.TPI.COR)[names(K1.LF.TPI.COR)=="Freq"] <- "Pearson_R2"
View(K1.LF.TPI.COR)


## which variables are less collinear?
subset(K1.LF.TPI.COR, abs(Pearson_R2) <= 0.7) 


## now check out the scatterplots for just the topographic position index
pairs(KOALA.VAR.CONTINUOUS.1[,grep("^lf_tpi", colnames(KOALA.VAR.CONTINUOUS.1))],
      #lower.panel = NULL, 
      lower.panel = panel.cor,
      col = "blue", pch = 19, cex = 0.7, main = "Topograpphic position")


## now need to consider how to choose the variables, if they are all pretty correlated 
## with each other...





#########################################################################################################################
## REMOTE IMAGERY: SEASONAL FRACTIONAL COVER
## pearson correlation for all landform variables
K1.RS.COR = cor(KOALA.VAR.CONTINUOUS.1[,grep("^rs", colnames(KOALA.VAR.CONTINUOUS.1))])


## sort correlation matrix
K1.RS.COR[lower.tri(K1.RS.COR, diag = TRUE)] = NA     # Prepare to drop duplicates and meaningless information
K1.RS.COR = as.data.frame(as.table(K1.RS.COR))        # Turn into a 3-column table
K1.RS.COR = na.omit(K1.RS.COR)                        # Get rid of the junk
K1.RS.COR = K1.RS.COR[order(-abs(K1.RS.COR$Freq)),]   # Sort by highest correlation (whether +ve or -ve)


## rename : don't need the permutation number
names(K1.RS.COR)[names(K1.RS.COR)=="Var1"] <- "LayerName"
names(K1.RS.COR)[names(K1.RS.COR)=="Var2"] <- "Layer_2"
names(K1.RS.COR)[names(K1.RS.COR)=="Freq"] <- "Pearson_R2"
View(K1.RS.COR)


## now check out the scatterplots for just the topographic position index
pairs(KOALA.VAR.CONTINUOUS.1[,grep("^rs", colnames(KOALA.VAR.CONTINUOUS.1))],
      #lower.panel = NULL, 
      lower.panel = panel.cor,
      col = "blue", pch = 19, cex = 0.7, main = "Remote imagery")


## these are uncorrelated, could test them all if biologically meaningful?





#########################################################################################################################
## REMOTE IMAGERY: SEASONAL FRACTIONAL COVER
## don't use win (winter) for north coast – too 
## many missing values (due to shadows) – was Ok for Western


## pearson correlation for all landform variables
K1.SFC.COR = cor(KOALA.VAR.CONTINUOUS.1[,grep("^sfc_sum", colnames(KOALA.VAR.CONTINUOUS.1))])


## sort correlation matrix
K1.SFC.COR[lower.tri(K1.SFC.COR, diag = TRUE)] = NA     # Prepare to drop duplicates and meaningless information
K1.SFC.COR = as.data.frame(as.table(K1.SFC.COR))        # Turn into a 3-column table
K1.SFC.COR = na.omit(K1.SFC.COR)                        # Get rid of the junk
K1.SFC.COR = K1.SFC.COR[order(-abs(K1.SFC.COR$Freq)),]   # Sort by highest correlation (whether +ve or -ve)


## rename : don't need the permutation number
names(K1.SFC.COR)[names(K1.SFC.COR)=="Var1"] <- "LayerName"
names(K1.SFC.COR)[names(K1.SFC.COR)=="Var2"] <- "Layer_2"
names(K1.SFC.COR)[names(K1.SFC.COR)=="Freq"] <- "Pearson_R2"
View(K1.SFC.COR)


## which variables are less collinear?
subset(K1.SFC.COR, abs(Pearson_R2) <= 0.7) 


## scatterplots for summer SFC layers
pairs(KOALA.VAR.CONTINUOUS.1[,grep("^sfc_sum", colnames(KOALA.VAR.CONTINUOUS.1))],
      #lower.panel = NULL, 
      lower.panel = panel.cor,
      col = "blue", pch = 19, cex = 0.7, main = "Summer fractional cover")


## scatterplots for spring SFC layers
pairs(KOALA.VAR.CONTINUOUS.1[,grep("^sfc_spr", colnames(KOALA.VAR.CONTINUOUS.1))],
      #lower.panel = NULL, 
      lower.panel = panel.cor,
      col = "blue", pch = 19, cex = 0.7)


## scatterplots for autumn SFC layers
pairs(KOALA.VAR.CONTINUOUS.1[,grep("^sfc_aut", colnames(KOALA.VAR.CONTINUOUS.1))],
      #lower.panel = NULL, 
      lower.panel = panel.cor,
      col = "blue", pch = 19, cex = 0.7)


## scatterplots for winter SFC layers
## don't use win (winter) for north coast – too many missing values (due to shadows) – was Ok for Western
pairs(KOALA.VAR.CONTINUOUS.1[,grep("^sfc_win", colnames(KOALA.VAR.CONTINUOUS.1))],
      #lower.panel = NULL, 
      lower.panel = panel.cor,
      col = "blue", pch = 19, cex = 0.7)





#########################################################################################################################
## SOIL: CREATE A FERTILITY SUBSET?


## pearson correlation for all landform variables
K1.SOIL.COR = cor(KOALA.VAR.CONTINUOUS.1[c(122:146)])
K1.FERT.COR = cor(KOALA.VAR.CONTINUOUS.1[c("gp_k_fillspl",
                                           "sp_ece000_100prop", "sp_ece100_200",
                                           "sp_nto000_100prop", "sp_nto100_200",
                                           "sp_pto000_100prop", "sp_pto100_200",
                                           "sp_soc000_100prop", "sp_soc100_200")])


## sort correlation matrix
K1.FERT.COR[lower.tri(K1.FERT.COR, diag = TRUE)] = NA     # Prepare to drop duplicates and meaningless information
K1.FERT.COR = as.data.frame(as.table(K1.FERT.COR))        # Turn into a 3-column table
K1.FERT.COR = na.omit(K1.FERT.COR)                        # Get rid of the junk
K1.FERT.COR = K1.FERT.COR[order(-abs(K1.FERT.COR$Freq)),]   # Sort by highest correlation (whether +ve or -ve)


## rename : don't need the permutation number
names(K1.FERT.COR)[names(K1.FERT.COR)=="Var1"] <- "LayerName"
names(K1.FERT.COR)[names(K1.FERT.COR)=="Var2"] <- "Layer_2"
names(K1.FERT.COR)[names(K1.FERT.COR)=="Freq"] <- "Pearson_R2"
View(K1.FERT.COR)


## which variables are less collinear?
subset(K1.FERT.COR, abs(Pearson_R2) <= 0.7)


## scatterplots for soil layers
pairs(KOALA.VAR.CONTINUOUS.1[c("gp_k_fillspl",
                               "sp_ece000_100prop", "sp_ece100_200",
                               "sp_nto000_100prop", "sp_nto100_200",
                               "sp_pto000_100prop", "sp_pto100_200",
                               "sp_soc000_100prop", "sp_soc100_200")],
      
      lower.panel = panel.cor,
      col = "blue", pch = 19, cex = 0.7, main = "Soil fertility")






#########################################################################################################################
## 3). Plot key variables for forested areas 
#########################################################################################################################


## plot key variables for each point, showing how the variables distributed across koala occurences


## Least collinear topography layers
ggplot() + 
  geom_polygon(data = forest_subset, aes(x = long, y = lat, group = group), fill = "grey70", 
               colour = "grey90", alpha = 1) +
  geom_point(aes(LONGITUDE, LATITUDE, colour = VEG_VARAIBLE_1), 
             data = WT.SITES, alpha = 1, size = 2) +
  scale_colour_gradient(low = "white", high = "red")


## Least collinear fertility layers
ggplot() + 
  geom_polygon(data = forest_subset, aes(x = long, y = lat, group = group), fill = "grey70", 
               colour = "grey90", alpha = 1) +
  geom_point(aes(LONGITUDE, LATITUDE, colour = VEG_VARAIBLE_1), 
             data = WT.SITES, alpha = 1, size = 2) +
  scale_colour_gradient(low = "white", high = "red")


## Least collinear tree dsitribution layers
ggplot() + 
  geom_polygon(data = forest_subset, aes(x = long, y = lat, group = group), fill = "grey70", 
               colour = "grey90", alpha = 1) +
  geom_point(aes(LONGITUDE, LATITUDE, colour = VEG_VARAIBLE_1), 
             data = WT.SITES, alpha = 1, size = 2) +
  scale_colour_gradient


## Least collinear fragmentation idices
ggplot() + 
  geom_polygon(data = forest_subset, aes(x = long, y = lat, group = group), fill = "grey70", 
               colour = "grey90", alpha = 1) +
  geom_point(aes(LONGITUDE, LATITUDE, colour = VEG_VARAIBLE_1), 
             data = WT.SITES, alpha = 1, size = 2) +
  scale_colour_gradient(low = "white", high = "red")





#########################################################################################################################
## Eventually extract values from raster layers in R
#########################################################################################################################


## Improved vegetation layers can be re-sampled to 250m in R: resample(r, s, method = 'bilinear'), but could just use ArcMap.
## then extract vegetation values at the Koala occurrences


## resample rasters to 250m, convert to ASCII, then read them in as a stack. There will be lots! EG:
raster.list        = list.files("N:/NVIS_DATA/NSW_Standard_Predictors/Continuous",  pattern = '\\.tif$')
categorical.list   = list.files("N:/NVIS_DATA/NSW_Standard_Predictors/Categorical", pattern = '\\.tif$')

length(raster.list) ## 159 files!
length(categorical.list)
raster.list
## write.csv(list(raster.list), "./data/base/raster/raster.list.csv", row.names = FALSE)

## groupings:
## Energy
## Temp
## Water
## Drainage
## Geophysics
## Landscape
## Location
## Remote imagery
## Soil


## need to check extraction works
n.env.variables   = length(raster.list)
site.env.mat      = SITES.points
env.grids         = raster.list

 
## loop over the raster list and extract the variables
for(i.env in 1:n.env.variables) {

  col.heads       = colnames(site.env.mat)
  Env1.ras        = raster(env.grids[i.env])  ## error is here, needs to be a c() of all the rasters...
  point.vals      = extract(Env1.ras, SITES.points, method = 'simple')
  site.env.mat    = cbind(site.env.mat, point.vals)
  colnames(site.env.mat) = c(col.heads, substr(env.grids[i.env], 1, (nchar(env.grids[i.env])-4)))

} ## end for i.env


## bind the sites to the raster values
site.env.mat    = cbind(SITES[,c("SITE")], site.env.mat)
SITES.ENV       = as.data.frame(site.env.mat)


## create dataframe of just the variables: use for colinearity analyses
# KOALA.SITES.CONTINUOUS = SITES.ENV[c(1:n.env.variables)]


## also can split up the points based on criteria....




#########################################################################################################################
############################################ LOTS OF OPTIONS ############################################################
#########################################################################################################################